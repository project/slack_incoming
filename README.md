Slack Incoming
==============

This module handles incoming and outgoing messages from and to Slack.

You can use the Slack API service to call any of the Slack API functions.

There are controller methods to handle incoming Slack events, including messages.
